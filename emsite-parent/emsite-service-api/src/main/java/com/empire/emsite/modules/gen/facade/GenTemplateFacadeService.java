/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.gen.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.gen.entity.GenTemplate;

/**
 * 类GenTemplateFacadeService.java的实现描述：代码模板FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:48:45
 */
public interface GenTemplateFacadeService {

    public GenTemplate get(String id);

    public Page<GenTemplate> find(Page<GenTemplate> page, GenTemplate genTemplate);

    public void save(GenTemplate genTemplate);

    public void delete(GenTemplate genTemplate);
}
