/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.sys.entity.Menu;
import com.empire.emsite.modules.sys.entity.Role;
import com.empire.emsite.modules.sys.entity.User;

/**
 * 类SystemFacadeService.java的实现描述：系统管理，安全相关实体的管理类,包括用户、角色、菜单.
 * 
 * @author arron 2017年9月17日 下午9:52:53
 */
public interface SystemFacadeService {

    /**
     * 获取用户
     * 
     * @param id
     * @return
     */
    public User getUser(String id);

    /**
     * 根据登录名获取用户
     * 
     * @param loginName
     * @return
     */
    public User getUserByLoginName(String loginName);

    public Page<User> findUser(Page<User> page, User user);

    /**
     * 无分页查询人员列表
     * 
     * @param user
     * @return
     */
    public List<User> findUser(User user);

    /**
     * 通过部门ID获取用户列表，仅返回用户id和name（树查询用户时用）
     * 
     * @param user
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<User> findUserByOfficeId(String officeId);

    public void saveUser(User user);

    public void updateUserInfo(User user);

    public void deleteUser(User user);

    public void updatePasswordById(String id, String loginName, String newPassword);

    public void updateUserLoginInfo(User user);

    /**
     * 获得活动会话
     * 
     * @return
     */
    /*
     * public Collection<Session> getActiveSessions(){ return
     * sessionDao.getActiveSessions(false); }
     */

    //-- Role Service --//

    public Role getRole(String id);

    public Role getRoleByName(String name);

    public Role getRoleByEnname(String enname);

    public List<Role> findRole(Role role);

    public List<Role> findAllRole(User user);

    public void saveRole(Role role);

    public void deleteRole(Role role);

    public Boolean outUserInRole(Role role, User user);

    public User assignUserToRole(Role role, User user);

    //-- Menu Service --//
    public Menu getMenu(String id);

    public List<Menu> findAllMenu(User user);

    public void saveMenu(Menu menu);

    public void updateMenuSort(Menu menu);

    public void deleteMenu(Menu menu);

    public List<Menu> findAllMenu();
}
