/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.gen.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.gen.entity.GenScheme;
import com.empire.emsite.modules.sys.entity.User;

/**
 * 类GenSchemeFacadeService.java的实现描述：生成方案FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:47:45
 */
public interface GenSchemeFacadeService {

    public GenScheme get(String id);

    public Page<GenScheme> find(Page<GenScheme> page, GenScheme genScheme);

    public String save(GenScheme genScheme, User user);

    public void delete(GenScheme genScheme);
}
